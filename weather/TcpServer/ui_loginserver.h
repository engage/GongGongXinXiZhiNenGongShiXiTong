/********************************************************************************
** Form generated from reading UI file 'loginserver.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINSERVER_H
#define UI_LOGINSERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_loginserver
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *userEdit_2;
    QLineEdit *passEdit_2;
    QWidget *layoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *loginBt_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *quitBt_2;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *loginserver)
    {
        if (loginserver->objectName().isEmpty())
            loginserver->setObjectName(QStringLiteral("loginserver"));
        loginserver->resize(420, 330);
        centralwidget = new QWidget(loginserver);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 421, 81));
        QFont font;
        font.setPointSize(36);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);
        layoutWidget = new QWidget(centralwidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(90, 110, 241, 91));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        userEdit_2 = new QLineEdit(layoutWidget);
        userEdit_2->setObjectName(QStringLiteral("userEdit_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(userEdit_2->sizePolicy().hasHeightForWidth());
        userEdit_2->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setPointSize(16);
        userEdit_2->setFont(font1);

        verticalLayout_2->addWidget(userEdit_2);

        passEdit_2 = new QLineEdit(layoutWidget);
        passEdit_2->setObjectName(QStringLiteral("passEdit_2"));
        sizePolicy.setHeightForWidth(passEdit_2->sizePolicy().hasHeightForWidth());
        passEdit_2->setSizePolicy(sizePolicy);
        passEdit_2->setFont(font1);
        passEdit_2->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(passEdit_2);

        layoutWidget_2 = new QWidget(centralwidget);
        layoutWidget_2->setObjectName(QStringLiteral("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(50, 230, 341, 61));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        loginBt_2 = new QPushButton(layoutWidget_2);
        loginBt_2->setObjectName(QStringLiteral("loginBt_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(loginBt_2->sizePolicy().hasHeightForWidth());
        loginBt_2->setSizePolicy(sizePolicy1);
        loginBt_2->setFont(font1);

        horizontalLayout_2->addWidget(loginBt_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        quitBt_2 = new QPushButton(layoutWidget_2);
        quitBt_2->setObjectName(QStringLiteral("quitBt_2"));
        sizePolicy1.setHeightForWidth(quitBt_2->sizePolicy().hasHeightForWidth());
        quitBt_2->setSizePolicy(sizePolicy1);
        quitBt_2->setFont(font1);

        horizontalLayout_2->addWidget(quitBt_2);

        loginserver->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(loginserver);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        loginserver->setStatusBar(statusbar);

        retranslateUi(loginserver);

        QMetaObject::connectSlotsByName(loginserver);
    } // setupUi

    void retranslateUi(QMainWindow *loginserver)
    {
        loginserver->setWindowTitle(QApplication::translate("loginserver", "MainWindow", 0));
        label->setText(QApplication::translate("loginserver", "\345\220\216\345\217\260\347\256\241\347\220\206\346\216\247\345\210\266\347\263\273\347\273\237", 0));
        userEdit_2->setPlaceholderText(QApplication::translate("loginserver", "\347\224\250\346\210\267", 0));
        passEdit_2->setPlaceholderText(QApplication::translate("loginserver", "\345\257\206\347\240\201", 0));
        loginBt_2->setText(QApplication::translate("loginserver", "\347\231\273\345\275\225", 0));
        quitBt_2->setText(QApplication::translate("loginserver", "\351\200\200\345\207\272", 0));
    } // retranslateUi

};

namespace Ui {
    class loginserver: public Ui_loginserver {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINSERVER_H
