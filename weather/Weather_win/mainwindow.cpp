﻿
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QWidget>
#include<QApplication>
#include<QMouseEvent>
#include<QDebug>
#include<QTextCodec>
#include <QFileDialog>
#include <QDate>
#include <QTime>
#include <QModelIndex>
#include<QPainter>
#include<QByteArray>
#include<QFont>
#include<QRect>
#include <QDataStream>
#include <synchapi.h>
#include <QWaitCondition>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    recvsize = 0;
    filesize = 0;
    ui->widget->hide();

    //初始化QTcpSocket对象
    msocket = new QTcpSocket();
    manager  = new QNetworkAccessManager();
    manager_pic  = new QNetworkAccessManager();
    downloadMg  = new QNetworkAccessManager();
    mtimer = new QTimer();
    timer = new QTimer(this);
    t = new QTimer(this);
    ntimer = new QTimer(this);
    dtimer = new QTimer(this);


    connect(msocket,SIGNAL(readyRead()),this, SLOT(read_msg())); //关联接收数据信号
    connect(msocket,SIGNAL(connected()),this, SLOT(on_videoBt_clicked()));//如果链接成功会发送connected信号
    connect(msocket,SIGNAL(disconnected()),this,SLOT(disconnect_from_server()));//如果掉线会发送disconnected信号
    connect(QApplication::focusWidget(),SIGNAL(selectionChanged),this,SLOT(on_lineEdit_selectionChanged()));
    connect(manager, SIGNAL(finished(QNetworkReply*)),this,SLOT(read_data(QNetworkReply*)));
    connect(manager_pic, SIGNAL(finished(QNetworkReply*)),this,SLOT(read_data_pic(QNetworkReply*)));
    connect(downloadMg, SIGNAL(finished(QNetworkReply*)),this,SLOT(read_pic(QNetworkReply*)));

    //定义一个结构体，存放串口的各个参数，波特率、数据位、校验位、停止位等
    struct PortSettings myComSetting = {BAUD115200,DATA_8,PAR_NONE,STOP_1,FLOW_OFF,500};
    //定义串口对象，并传递参数，在构造函数中对其进行初始化
    myCom = new Win_QextSerialPort("com6",myComSetting,QextSerialBase::EventDriven);
    //以可读可写的方式打开串口文件
    myCom->open(QIODevice::ReadWrite);
    connect(myCom,SIGNAL(readyRead()),this,SLOT(readMyCom()));
    //信号和槽函数关联，当缓冲区有数据时，进行读串口操作
    connect(mtimer,SIGNAL(timeout()),this,SLOT(update_time()));
    connect(mtimer,SIGNAL(timeout()),this, SLOT(request_pic()));

    //定义Qt中的进程
    mprocess = new QProcess();
    mtimer->start(100);

    connect(timer,  SIGNAL(timeout()),  this,  SLOT(scrollCaption()));
    connect(timer,  SIGNAL(timeout()),  this,  SLOT(scrollCaptionto()));
    // 定时150毫秒
    timer->start(150);

    t->stop();
    connect(t,SIGNAL(timeout()),this,SLOT(wordtimer()));
    connect(t,SIGNAL(timeout()),this,SLOT(wordtimerto()));
    t->start(10);

    connect(ntimer,SIGNAL(timeout()),this,SLOT(show_time()));
    ntimer->start(100);

    connect(dtimer,SIGNAL(timeout()),this,SLOT( visitors_flowrate()));
    dtimer->start(15000);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_videoBt_clicked()
{

    if(ui->videoBt->text() == "关闭")
       {
           msocket->disconnectFromHost();
           return ;
       }
       QString ip = "192.168.0.60";
       int port = 2222;
       msocket->connectToHost(ip,port);

}

/*
读取视频
*/

void MainWindow::read_msg()
{
    QString msg = msocket->readAll();
     if(msg.contains(".avi",Qt::CaseSensitive))
    {
       QString cmd =QString("mplayer -slave -quiet -geometry 241:43 -zoom -x %1 -y %2 http://192.168.199.251/%3")
               .arg(QString::number(ui->videolabel->width()))
               .arg(QString::number(ui->videolabel->height()))
               .arg(msg);
        //启动进程播放
        //判断进程是否在运行，
         if(mprocess->state()==QProcess::Running)
         {
             mprocess->kill();//杀死进程
             //等待
             mprocess->waitForFinished();
         }
         qDebug()<<cmd;
         mprocess->start(cmd);

     }
     else
     {
        ui->msglabel->setText(msg);

     }
}

/*
链接服务器成功
*/
void MainWindow::connect_to_server()
{

    ui->videoBt->setText("关闭");

}

/*
断开服务器
*/
void MainWindow::disconnect_from_server()
{
    ui->videoBt->setText("链接");
}


void MainWindow::on_lineEdit_selectionChanged()
{
    ui->widget->show();
    ui->datelb->hide();
    ui->timelb->hide();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
    int x=e->x();
    int y=e->y();
    qDebug()<<x<<","<<y;

    if(x>131||x<0||y>52||y<22)
    {
        ui->widget->hide();
        ui->datelb->show();
        ui->timelb->show();
    }

}
void MainWindow::on_queryBt_clicked()
{

   ch = ui->lineEdit->text();
   if(ch!="重庆")
   {
    event=1;
   }
   qDebug()<<ch;

   QString rurl = QString ("http://www.sojson.com/open/api/weather/json.shtml?city=%1").arg(ch);
   QUrl url = rurl;
   // QUrl iurl("http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=3");
  QUrl iurl("http://image.baidu.com/channel/listjson?pn=0&rn=130&tag1=动物&tag2=全部&ie=utf8");
 //QUrl iurl( "http://image.baidu.com/channel/listjson?pn=0&rn=50&tag1=明星&tag2=全部&ie=utf8");
   QNetworkRequest request(url);//� �������
   manager->get(request);
   QNetworkRequest irequest(iurl);
   manager_pic->get(irequest);
}

void MainWindow::read_data(QNetworkReply *reply)
{

   QByteArray jdata = reply->readAll();
   QJsonParseError err;
   QJsonDocument doc = QJsonDocument::fromJson(jdata,&err);
   if(err.error != QJsonParseError::NoError)
   {
       qDebug()<<"error";
       return ;
   }

  QJsonObject obj = doc.object();
  QJsonObject objData = obj.value("data").toObject();
  QString pm25 = objData.value("pm25").toString();
  qDebug()<<"PM:"<<pm25;
  QJsonArray arr = objData.value("forecast").toArray();
  QJsonObject arr_obj = arr.at(0).toObject();

  QString date = arr_obj.value("date").toString();
  QString type = arr_obj.value("type").toString();
  QString sunrise = arr_obj.value("sunrise").toString();
          high = arr_obj.value("high").toString();
          low = arr_obj.value("low").toString();
  QString sunset = arr_obj.value("sunset").toString();
  QString aqi = arr_obj.value("aqi").toString();
  QString fx = arr_obj.value("fx").toString();
  QString fl = arr_obj.value("fl").toString();
  QString notice = arr_obj.value("notice").toString();
  QString wreater = QString("%1 %2 %3 %4 %5 %6 %7 %8 %9 %10")
          .arg(date).arg(type)
          .arg(sunrise).arg(high)
          .arg(low).arg(sunset)
          .arg(aqi).arg(fx)
          .arg(fl).arg(notice);
          qDebug()<<wreater;
          ui->showEdit->setText(wreater);
}


 void MainWindow::read_data_pic(QNetworkReply*reply)
 {

     QByteArray jdata = reply->readAll();
     //解析json数据
     QJsonParseError err;
     QJsonDocument doc = QJsonDocument::fromJson(jdata,&err);//检测json格式
     if(err.error != QJsonParseError::NoError)
     {
        qDebug()<<"error";
         return ;
     }

     QJsonObject obj = doc.object();

     QJsonArray array = obj.value("data").toArray();
     for(int i=0;i<array.size();i++)
     {
        QJsonObject arr_obj = array.at(i).toObject();
         //取对象中的名字abs， 图片路径image_url
        QString image = arr_obj.value("image_url").toString();
        picPaths.append(image);
        QJsonArray array = obj.value("images").toArray();

        //获取数组
         //取数组每一个项数据
        for(int i=0; i<array.size(); i++)
       {

             QJsonObject arr_obj = array.at(i).toObject();
             QString copyright = arr_obj.value("copyright").toString();
             QString enddate = arr_obj.value("enddate").toString();
             QString iurl = arr_obj.value("url").toString();

           qDebug()<<copyright<<enddate<<iurl;

         picPaths.append(iurl);
     }
 }

 }


void MainWindow::read_pic(QNetworkReply*reply)
{

       //启动定时器
       mtimer->start(1000);
       static int i=0;
       QByteArray array = reply->readAll();
       qDebug()<<array;
       QPixmap pix;
       pix.loadFromData(array);
       i++;
       pix = pix.scaled(ui->label->size(),Qt::KeepAspectRatio);
       ui->label->setPixmap(pix);

}

//定时请求
void MainWindow::request_pic()
{
    static int i = 0;
    if(i < picPaths.size())
    {
        QString image = picPaths.at(i);
        qDebug()<<image;
        QUrl iurl(image);
        QNetworkRequest request(iurl);//请求对象
        downloadMg->get(request);//发出get请求
        i++;
    }else
    i=0;
}


//定时刷新时间函数
void MainWindow::update_time()
{
    ui->datelb->setText(QDate::currentDate().toString("yyyy-MM-dd"));
    ui->timelb->setText(QTime::currentTime().toString("hh:mm:ss"));
}

void MainWindow::scrollCaption()
{
    QString msg =  ui->msglabel->text();
    strScrollCation = QString(msg);
    static int nPos = 0;
    // 当截取的位置比字符串长时，从头开始
    if (nPos > strScrollCation.length())
        nPos = 0;
    ui->msglabel->setText(strScrollCation);

   //设置字体大小
    wordfont.setPointSize(20);
    ui->msglabel->setFont(wordfont);
    QString str = ui->msglabel->text();
    QFontMetrics fm(wordfont);
    witdth= fm.width(str);//将字符串长度转换为像素宽度大小
    hight= fm.height();//获取字体高度像素大小
    ui->msglabel->setText(str);
    ui->msglabel->setAlignment(Qt::AlignRight);
    ui->msglabel->resize(witdth,hight);// 设置主窗体大小
    nPos++;
}
void MainWindow::wordtimer()
{
   int  x=ui->msglabel->x();//获取labelx坐标点
   ui->msglabel->move(x-1,ui->msglabel->y());//每隔相应时间向左移动label
   if(x<=0-witdth)
   {
     ui->msglabel->setGeometry(QRect(710,ui->msglabel->y(), 710, hight));
   }

}

void MainWindow::readMyCom()//读串口函数
{
    bool ok;
    QByteArray temp = myCom->readAll();
    //读取串口缓冲区的所有数据给临时变量temp
     qDebug()<<temp;
     QString buf = QString(temp);
      qDebug()<<"buf="<<buf;
    //ui->textBrowser->insertPlainText(buf);
    //将串口的数据显示在窗口的文本浏览器中

    QString temperature = buf.section('-',0,0);
    QString humidity    = buf.section('-',1,1);
    QString disdance    = buf.section('-',2,2);

    if(!high.isEmpty()&&!low.isEmpty()&&!temperature.isEmpty()&&!humidity.isEmpty()&&!disdance.isEmpty())
    {
        qDebug()<<"湿度"<<temperature;
        qDebug()<<"温度"<<humidity;
        qDebug()<<"距离"<<disdance;
        qDebug()<<"high"<<high;
        qDebug()<<"low"<<low;
        QString high1 = high.section(' ',1,1);
        QString low1  = low.section(' ',1,1);

        QString high2 = high1.section('.',0,0);
        QString  low2 = low1.section('.',0,0);

        int temperature_value = temperature.toFloat(&ok);
        int humidity_value    = humidity.toFloat(&ok);
        int disdance_value    = disdance.toDouble(&ok);
        int high_value        = high2.toFloat(&ok);
        int low_value         = low2.toFloat(&ok);

        qDebug()<<"temperature_value"<<temperature_value;
        qDebug()<<"humidity_value"<<humidity_value;
        qDebug()<<"disdance_value"<<disdance_value;
        qDebug()<<"high_value"<<high_value;
        qDebug()<<"low_value"<<low_value;

        if(temperature_value<low_value||temperature_value>high_value)
        {
                  if(disdance_value<=100)
                {
                    j++;
                 }
                  if(dis_event==1)
                   {
                        dis_number=j*6;
                        if(dis_number<=10)
                        {
                            QString buf =QString("温馨提示：当前地段人流较少，可放心大胆的往前走！实时人流统计： %1 人次/分钟 ")
                                    .arg(dis_number);
                            ui->promptlabel->setText(buf);
                           dtimer->stop();
                          sleep(5000);
                          qDebug()<<"go to there\r\n";
                           dtimer->start(15000);
                            j=0;
                            dis_number=0;
                            dis_event=0;
                        }
                        else if(dis_number>=45)
                        {

                            QString buf =QString("温馨提示：当前地段人流较为密集，请放慢脚步，注意安全！实时人流统计： %1 人次/分钟 ")
                                    .arg(dis_number);
                            ui->promptlabel->setText(buf);
                            dtimer->stop();
                           sleep(5000);
                            qDebug()<<"go to there\r\n";
                          dtimer->start(15000);
                            j=0;
                            dis_number=0;
                            dis_event=0;

                        }
                    }

                    QString buf =QString("温馨提示：实时气温与气象预报出现较大偏差，请您留意！ 环境温度为 %1.0。C 环境湿度为 %2.0 ")
                            .arg(temperature_value)
                            .arg(humidity_value);
                    ui->promptlabel->setText(buf);


        }
    }
}

void MainWindow::scrollCaptionto()
{
    QString msg =  ui->promptlabel->text();
    strScrollCation = QString(msg);
    static int nPos = 0;
    // 当截取的位置比字符串长时，从头开始
    if (nPos > strScrollCation.length())
        nPos = 0;
    ui->promptlabel->setText(strScrollCation);

   //设置字体大小
    wordfont.setPointSize(20);
    ui->promptlabel->setFont(wordfont);
    QString str = ui->promptlabel->text();
    QFontMetrics fm(wordfont);
    witdth= fm.width(str);//将字符串长度转换为像素宽度大小
    hight= fm.height();//获取字体高度像素大小
    ui->promptlabel->setText(str);
    ui->promptlabel->setAlignment(Qt::AlignRight);
    ui->promptlabel->resize(witdth,hight);// 设置主窗体大小
    nPos++;
}
void MainWindow::wordtimerto()
{
   int  x=ui->promptlabel->x();//获取labelx坐标点
   ui->promptlabel->move(x-1,ui->promptlabel->y());//每隔相应时间向左移动label
   if(x<=0-witdth)
   {
     ui->promptlabel->setGeometry(QRect(251,ui->promptlabel->y(), 251, hight));//第一个参数：label左端向左开始滑动的起始横坐标
   }

}


void MainWindow::show_time(void)
{
    if(event==1)
    {
        i+=100;
        qDebug()<<"go to there\r\n";
      if(i >3000)
      {
          ui->lineEdit->setText("重庆");
          ch ="重庆";
          QString rurl = QString ("http://www.sojson.com/open/api/weather/json.shtml?city=%1").arg(ch);
          QUrl url = rurl;
          QNetworkRequest request(url);//������
          manager->get(request);
          ntimer->stop();
      event=0;
      }

    }
}

void MainWindow::visitors_flowrate()
{
    dis_event=1;
}


void MainWindow::sleep(unsigned int  msec)
{
    QTime t;
        t.start();
        while(t.elapsed()<msec)
            QCoreApplication::processEvents();
}




