/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>
#include <softkey.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QPushButton *queryBt;
    QTextEdit *showEdit;
    QLabel *label;
    QLabel *datelb;
    QLabel *timelb;
    SoftKey *widget;
    QPushButton *videoBt;
    QLabel *msglabel;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *videolabel;
    QLabel *label_4;
    QLabel *promptlabel;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 480);
        MainWindow->setMinimumSize(QSize(800, 480));
        MainWindow->setMaximumSize(QSize(800, 480));
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(0, 0, 133, 41));
        queryBt = new QPushButton(centralWidget);
        queryBt->setObjectName(QStringLiteral("queryBt"));
        queryBt->setGeometry(QRect(130, 0, 61, 41));
        showEdit = new QTextEdit(centralWidget);
        showEdit->setObjectName(QStringLiteral("showEdit"));
        showEdit->setGeometry(QRect(190, 0, 611, 41));
        showEdit->setStyleSheet(QLatin1String("background-color: rgb(255, 170, 127);\n"
"color: rgb(0, 85, 255);"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 190, 251, 221));
        label->setStyleSheet(QStringLiteral(""));
        datelb = new QLabel(centralWidget);
        datelb->setObjectName(QStringLiteral("datelb"));
        datelb->setGeometry(QRect(0, 50, 181, 41));
        datelb->setStyleSheet(QLatin1String("background-color: rgb(170, 255, 255);\n"
"font: 17pt \"Arial\";\n"
"background-color: rgb(85, 255, 127);"));
        timelb = new QLabel(centralWidget);
        timelb->setObjectName(QStringLiteral("timelb"));
        timelb->setGeometry(QRect(0, 90, 181, 51));
        timelb->setStyleSheet(QLatin1String("background-color: rgb(170, 255, 255);\n"
"background-color: rgb(85, 255, 127);\n"
"font: 17pt \"Arial\";"));
        widget = new SoftKey(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 50, 241, 138));
        videoBt = new QPushButton(centralWidget);
        videoBt->setObjectName(QStringLiteral("videoBt"));
        videoBt->setGeometry(QRect(710, 430, 91, 41));
        msglabel = new QLabel(centralWidget);
        msglabel->setObjectName(QStringLiteral("msglabel"));
        msglabel->setGeometry(QRect(740, 430, 110, 31));
        msglabel->setStyleSheet(QStringLiteral(""));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, 40, 251, 371));
        label_2->setStyleSheet(QStringLiteral("background-color: rgb(170, 170, 127);"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(-7, 410, 811, 71));
        label_3->setStyleSheet(QLatin1String("font: 9pt \"Arial\";\n"
"background-color: rgb(255, 170, 127);"));
        videolabel = new QLabel(centralWidget);
        videolabel->setObjectName(QStringLiteral("videolabel"));
        videolabel->setGeometry(QRect(260, 40, 531, 371));
        videolabel->setStyleSheet(QStringLiteral(""));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(773, 40, 31, 371));
        label_4->setStyleSheet(QStringLiteral("background-color: rgb(255, 170, 127);"));
        promptlabel = new QLabel(centralWidget);
        promptlabel->setObjectName(QStringLiteral("promptlabel"));
        promptlabel->setGeometry(QRect(0, 150, 251, 31));
        promptlabel->setStyleSheet(QLatin1String("font: 75 20pt \"Times New Roman\";\n"
"color: rgb(170, 85, 127);"));
        MainWindow->setCentralWidget(centralWidget);
        label_2->raise();
        promptlabel->raise();
        label_3->raise();
        msglabel->raise();
        lineEdit->raise();
        queryBt->raise();
        showEdit->raise();
        label->raise();
        datelb->raise();
        timelb->raise();
        widget->raise();
        videoBt->raise();
        label_4->raise();
        videolabel->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        lineEdit->setText(QApplication::translate("MainWindow", "\351\207\215\345\272\206", 0));
        lineEdit->setPlaceholderText(QApplication::translate("MainWindow", "\345\237\216\345\270\202", 0));
        queryBt->setText(QApplication::translate("MainWindow", "\346\237\245\350\257\242", 0));
        label->setText(QString());
        datelb->setText(QString());
        timelb->setText(QString());
        videoBt->setText(QApplication::translate("MainWindow", "\351\223\276\346\216\245", 0));
        msglabel->setText(QString());
        label_2->setText(QString());
        label_3->setText(QString());
        videolabel->setText(QString());
        label_4->setText(QString());
        promptlabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
