﻿#include "inbutton.h"
#include <QApplication>
#include <QLineEdit>
#include <QKeyEvent>
#include<QDebug>
InButton::InButton(QWidget *parent):QToolButton(parent)
{

}

InButton::~InButton()
{

}

//鼠标点击事件
void InButton::mousePressEvent(QMouseEvent *e)
{
    qDebug()<<"HAHAHAHH";
    QString sty = QString("border-image: url(:%1);").arg(press_pic);
    this->setStyleSheet(sty);
    QToolButton::mousePressEvent(e);

}

//鼠标释放事件
void InButton::mouseReleaseEvent(QMouseEvent *e)
{

    //检测是否点击删按钮
    if(this->text() == "<<")
    {
        //获取焦点控件
        QLineEdit *edit = (QLineEdit*)QApplication::focusWidget();
        QString ch = edit->text();
        ch.remove(ch.size()-1, 1);
        edit->setText(ch);
    }
    else
    {

    //获取焦点控件

    QLineEdit *edit = (QLineEdit*)QApplication::focusWidget();
    QString ch = edit->text();
    ch.append(this->text());
     qDebug()<<ch;
    edit->setText(ch);
    if(ch=="chongqing")
        edit->setText("重庆");
    else if(ch=="chengdu")
         edit->setText("成都");
    else if(ch=="shanghai")
         edit->setText("上海");
    else if(ch=="wuhan")
         edit->setText("武汉");
    else if(ch=="xian")
         edit->setText("西安");
    else if(ch=="changsha")
         edit->setText("长沙");
    else if(ch=="jinan")
         edit->setText("济南");
    else if(ch=="nanning")
         edit->setText("南宁");
    else if(ch=="kunming")
         edit->setText("昆明");
    else if(ch=="shijiazhuang")
         edit->setText("石家庄");
    else if(ch=="beijing")
         edit->setText("北京");
    else if(ch=="changchun")
         edit->setText("长春");
    else if(ch=="shenzhen")
         edit->setText("深圳");
    else if(ch=="tianjin")
         edit->setText("天津");
    else if(ch=="wulumuqi")
         edit->setText("乌鲁木齐");
    else if(ch=="haerbin")
         edit->setText("哈尔滨");
    else if(ch=="shenyang")
         edit->setText("沈阳");
    else if(ch=="nanchang")
         edit->setText("兰州");
    else if(ch=="guiuyang")
         edit->setText("贵阳");
    else if(ch=="nanchang")
         edit->setText("南昌");
    else if(ch=="zhengzhou")
         edit->setText("郑州");
    else if(ch=="huhehaote")
         edit->setText("呼和浩特");
    else if(ch=="xianggang")
         edit->setText("香港");
    else if(ch=="aomen")
         edit->setText("澳门");



    }
#if 0
    //事件实现软键盘功能（模拟按键事件）
    QString ch = this->text();
    qDebug()<<ch;
    char keych = ch.at(0).toLatin1();//从QString取出第一个字符QChar转成char
    //获取删除按钮的键值
    QKeyEvent *key = NULL;
    if(this->text() == "<<")
    {
       //创建BackSpace按钮事件
       key = new QKeyEvent(QEvent::KeyPress,Qt::Key_Backspace, Qt::NoModifier);
    }
   else if(this->text() == "英")
    {
        this->setText("中");

    }
    else
    {
        //创建按钮事件
        key = new QKeyEvent(QEvent::KeyPress,keych, Qt::NoModifier,ch);

    }
    #endif

    //发送按钮事件
  //  QApplication::postEvent(QApplication::focusWidget(), );


    QString sty = QString("border-image: url(:%1);").arg(release_pic);
    this->setStyleSheet(sty);
    QToolButton::mouseReleaseEvent(e);
}


void InButton::setButtonBackground(QString press, QString release)
{
    this->press_pic = press;
    this->release_pic = release;
}
