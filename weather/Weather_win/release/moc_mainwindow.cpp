/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[24];
    char stringdata[308];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 9), // "read_data"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 14), // "QNetworkReply*"
QT_MOC_LITERAL(4, 37, 5), // "reply"
QT_MOC_LITERAL(5, 43, 28), // "on_lineEdit_selectionChanged"
QT_MOC_LITERAL(6, 72, 11), // "update_time"
QT_MOC_LITERAL(7, 84, 18), // "on_queryBt_clicked"
QT_MOC_LITERAL(8, 103, 13), // "read_data_pic"
QT_MOC_LITERAL(9, 117, 8), // "read_pic"
QT_MOC_LITERAL(10, 126, 11), // "request_pic"
QT_MOC_LITERAL(11, 138, 8), // "read_msg"
QT_MOC_LITERAL(12, 147, 18), // "on_videoBt_clicked"
QT_MOC_LITERAL(13, 166, 17), // "connect_to_server"
QT_MOC_LITERAL(14, 184, 22), // "disconnect_from_server"
QT_MOC_LITERAL(15, 207, 13), // "scrollCaption"
QT_MOC_LITERAL(16, 221, 9), // "wordtimer"
QT_MOC_LITERAL(17, 231, 9), // "readMyCom"
QT_MOC_LITERAL(18, 241, 9), // "show_time"
QT_MOC_LITERAL(19, 251, 15), // "scrollCaptionto"
QT_MOC_LITERAL(20, 267, 11), // "wordtimerto"
QT_MOC_LITERAL(21, 279, 17), // "visitors_flowrate"
QT_MOC_LITERAL(22, 297, 5), // "sleep"
QT_MOC_LITERAL(23, 303, 4) // "msec"

    },
    "MainWindow\0read_data\0\0QNetworkReply*\0"
    "reply\0on_lineEdit_selectionChanged\0"
    "update_time\0on_queryBt_clicked\0"
    "read_data_pic\0read_pic\0request_pic\0"
    "read_msg\0on_videoBt_clicked\0"
    "connect_to_server\0disconnect_from_server\0"
    "scrollCaption\0wordtimer\0readMyCom\0"
    "show_time\0scrollCaptionto\0wordtimerto\0"
    "visitors_flowrate\0sleep\0msec"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x08 /* Private */,
       5,    0,  112,    2, 0x08 /* Private */,
       6,    0,  113,    2, 0x08 /* Private */,
       7,    0,  114,    2, 0x08 /* Private */,
       8,    1,  115,    2, 0x08 /* Private */,
       9,    1,  118,    2, 0x08 /* Private */,
      10,    0,  121,    2, 0x08 /* Private */,
      11,    0,  122,    2, 0x08 /* Private */,
      12,    0,  123,    2, 0x08 /* Private */,
      13,    0,  124,    2, 0x08 /* Private */,
      14,    0,  125,    2, 0x08 /* Private */,
      15,    0,  126,    2, 0x08 /* Private */,
      16,    0,  127,    2, 0x08 /* Private */,
      17,    0,  128,    2, 0x08 /* Private */,
      18,    0,  129,    2, 0x08 /* Private */,
      19,    0,  130,    2, 0x08 /* Private */,
      20,    0,  131,    2, 0x08 /* Private */,
      21,    0,  132,    2, 0x08 /* Private */,
      22,    1,  133,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UInt,   23,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->read_data((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 1: _t->on_lineEdit_selectionChanged(); break;
        case 2: _t->update_time(); break;
        case 3: _t->on_queryBt_clicked(); break;
        case 4: _t->read_data_pic((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 5: _t->read_pic((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 6: _t->request_pic(); break;
        case 7: _t->read_msg(); break;
        case 8: _t->on_videoBt_clicked(); break;
        case 9: _t->connect_to_server(); break;
        case 10: _t->disconnect_from_server(); break;
        case 11: _t->scrollCaption(); break;
        case 12: _t->wordtimer(); break;
        case 13: _t->readMyCom(); break;
        case 14: _t->show_time(); break;
        case 15: _t->scrollCaptionto(); break;
        case 16: _t->wordtimerto(); break;
        case 17: _t->visitors_flowrate(); break;
        case 18: _t->sleep((*reinterpret_cast< uint(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
