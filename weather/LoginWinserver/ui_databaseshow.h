/********************************************************************************
** Form generated from reading UI file 'databaseshow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATABASESHOW_H
#define UI_DATABASESHOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DatabaseShow
{
public:
    QWidget *centralwidget;
    QPushButton *Display_allBt;
    QListWidget *listWidget;
    QLabel *label;
    QTableView *tableView;
    QPushButton *backBt;
    QPushButton *exitBt;
    QLabel *label_2;

    void setupUi(QMainWindow *DatabaseShow)
    {
        if (DatabaseShow->objectName().isEmpty())
            DatabaseShow->setObjectName(QStringLiteral("DatabaseShow"));
        DatabaseShow->resize(560, 337);
        DatabaseShow->setMinimumSize(QSize(560, 337));
        DatabaseShow->setMaximumSize(QSize(560, 425));
        DatabaseShow->setStyleSheet(QStringLiteral("background-image: url(:/new/prefix1/tupian/1.jpg);"));
        centralwidget = new QWidget(DatabaseShow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        Display_allBt = new QPushButton(centralwidget);
        Display_allBt->setObjectName(QStringLiteral("Display_allBt"));
        Display_allBt->setGeometry(QRect(10, 10, 71, 21));
        Display_allBt->setStyleSheet(QStringLiteral("background-color: rgb(82, 151, 255);"));
        listWidget = new QListWidget(centralwidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 60, 111, 211));
        listWidget->setStyleSheet(QStringLiteral("background-image: url(:/new/prefix1/tupian/14.jpg);"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 40, 121, 21));
        tableView = new QTableView(centralwidget);
        tableView->setObjectName(QStringLiteral("tableView"));
        tableView->setGeometry(QRect(130, 10, 431, 261));
        tableView->setStyleSheet(QLatin1String("gridline-color: rgb(85, 255, 255);\n"
"background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:0.5, fx:0.5, fy:0.5, stop:0 rgba(0, 0, 0, 0), stop:0.52 rgba(0, 0, 0, 0), stop:0.565 rgba(82, 121, 76, 33), stop:0.65 rgba(159, 235, 148, 64), stop:0.721925 rgba(255, 238, 150, 129), stop:0.77 rgba(255, 128, 128, 204), stop:0.89 rgba(191, 128, 255, 64), stop:1 rgba(0, 0, 0, 0));\n"
"selection-background-color: rgb(85, 255, 255);\n"
"background-color: rgb(170, 170, 127);\n"
"border-color: rgb(170, 255, 255);\n"
""));
        backBt = new QPushButton(centralwidget);
        backBt->setObjectName(QStringLiteral("backBt"));
        backBt->setGeometry(QRect(0, 290, 91, 41));
        backBt->setStyleSheet(QStringLiteral("background-color: rgb(82, 151, 255);"));
        exitBt = new QPushButton(centralwidget);
        exitBt->setObjectName(QStringLiteral("exitBt"));
        exitBt->setGeometry(QRect(470, 290, 91, 41));
        exitBt->setStyleSheet(QStringLiteral("background-color: rgb(82, 151, 255);"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, -110, 571, 551));
        label_2->setStyleSheet(QStringLiteral("image: url(:/new/prefix1/tupian/1.jpg);"));
        DatabaseShow->setCentralWidget(centralwidget);
        label_2->raise();
        Display_allBt->raise();
        listWidget->raise();
        label->raise();
        tableView->raise();
        backBt->raise();
        exitBt->raise();

        retranslateUi(DatabaseShow);

        QMetaObject::connectSlotsByName(DatabaseShow);
    } // setupUi

    void retranslateUi(QMainWindow *DatabaseShow)
    {
        DatabaseShow->setWindowTitle(QApplication::translate("DatabaseShow", "MainWindow", 0));
        Display_allBt->setText(QApplication::translate("DatabaseShow", "\346\230\276\347\244\272\346\211\200\346\234\211", 0));
        label->setText(QString());
        backBt->setText(QApplication::translate("DatabaseShow", "\350\277\224\345\233\236", 0));
        exitBt->setText(QApplication::translate("DatabaseShow", "\346\263\250\351\224\200", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DatabaseShow: public Ui_DatabaseShow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATABASESHOW_H
