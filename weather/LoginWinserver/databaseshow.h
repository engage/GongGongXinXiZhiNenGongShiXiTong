#ifndef DATABASESHOW_H
#define DATABASESHOW_H

#include <QMainWindow>
#include "tcpserver.h"
#include <QContextMenuEvent>

namespace Ui {
class DatabaseShow;
}

class DatabaseShow : public QMainWindow
{
    Q_OBJECT
public:
    explicit DatabaseShow(QWidget *parent = 0);

    ~DatabaseShow();
#if 0
protected:
     void contextMenuEvent(QContextMenuEvent *event);
#endif
private slots:
    void on_Display_allBt_clicked();
    void on_listWidget_customContextMenuRequested(const QPoint &pos);
    void deleteSeedslot();
    void clearSeedslot();
    void on_listWidget_doubleClicked(const QModelIndex &index);
    void on_backBt_clicked();
    void on_exitBt_clicked();
private:
    Ui::DatabaseShow *ui;

};
#endif
