#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QList>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSql>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlDriver>

extern QSqlDatabase database;

namespace Ui {
class TcpServer;
}

class TcpServer : public QMainWindow
{
    Q_OBJECT

public:
    explicit TcpServer(QWidget *parent = 0);
    ~TcpServer();

private slots:
    void on_startBt_clicked();
    void new_client();
    void on_sendBt_clicked();
    void read_data();
    void client_dis();
    void on_cancelBt_clicked();
    void on_view_databaseBt_clicked();

private:
    Ui::TcpServer *ui;
    QTcpServer* mserver;
    QTcpSocket* msocket;
    QList<QTcpSocket *> sockets;

};

#endif // TCPSERVER_H
