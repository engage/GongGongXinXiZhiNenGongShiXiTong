#-------------------------------------------------
#
# Project created by QtCreator 2017-12-07T13:54:29
#
#-------------------------------------------------


QT       += core gui network
QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LoginWinserver
TEMPLATE = app


SOURCES += main.cpp\
        loginwinserver.cpp \
    tcpserver.cpp \
    databaseshow.cpp

HEADERS  += loginwinserver.h \
    tcpserver.h \
    databaseshow.h

FORMS    += loginwinserver.ui \
    tcpserver.ui \
    databaseshow.ui

DISTFILES += \
    tupian/1.jpg \
    tupian/2.jpg \
    tupian/3.jpg \
    tupian/4.jpg \
    tupian/5.jpg \
    tupian/6.jpg \
    tupian/7.jpg \
    tupian/8.jpg \
    tupian/9.jpg \
    tupian/10.jpg \
    tupian/11.jpg \
    tupian/12.jpg \
    tupian/13.jpg \
    tupian/14.jpg \
    tupian/15.jpg

RESOURCES += \
    image.qrc
