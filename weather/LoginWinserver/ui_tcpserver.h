/********************************************************************************
** Form generated from reading UI file 'tcpserver.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TCPSERVER_H
#define UI_TCPSERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TcpServer
{
public:
    QPushButton *sendBt;
    QTextEdit *sendEdit;
    QListWidget *recvList;
    QPushButton *sendBt_2;
    QListWidget *listWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QLineEdit *portEdit;
    QPushButton *startBt;
    QPushButton *view_databaseBt;
    QPushButton *cancelBt;

    void setupUi(QWidget *TcpServer)
    {
        if (TcpServer->objectName().isEmpty())
            TcpServer->setObjectName(QStringLiteral("TcpServer"));
        TcpServer->setWindowModality(Qt::NonModal);
        TcpServer->setEnabled(true);
        TcpServer->resize(560, 375);
        TcpServer->setMinimumSize(QSize(560, 375));
        TcpServer->setMaximumSize(QSize(560, 375));
        TcpServer->setStyleSheet(QLatin1String("background-image: url(:/new/prefix1/tupian/14.jpg);\n"
"background-image: url(:/new/prefix1/tupian/8.jpg);"));
        sendBt = new QPushButton(TcpServer);
        sendBt->setObjectName(QStringLiteral("sendBt"));
        sendBt->setGeometry(QRect(426, 343, 75, 23));
        sendEdit = new QTextEdit(TcpServer);
        sendEdit->setObjectName(QStringLiteral("sendEdit"));
        sendEdit->setGeometry(QRect(295, 191, 256, 146));
        recvList = new QListWidget(TcpServer);
        recvList->setObjectName(QStringLiteral("recvList"));
        recvList->setGeometry(QRect(295, 40, 256, 145));
        sendBt_2 = new QPushButton(TcpServer);
        sendBt_2->setObjectName(QStringLiteral("sendBt_2"));
        sendBt_2->setGeometry(QRect(295, 343, 75, 23));
        listWidget = new QListWidget(TcpServer);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(9, 40, 256, 192));
        listWidget->setStyleSheet(QStringLiteral(""));
        layoutWidget = new QWidget(TcpServer);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(9, 9, 191, 25));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        portEdit = new QLineEdit(layoutWidget);
        portEdit->setObjectName(QStringLiteral("portEdit"));

        horizontalLayout->addWidget(portEdit);

        startBt = new QPushButton(layoutWidget);
        startBt->setObjectName(QStringLiteral("startBt"));

        horizontalLayout->addWidget(startBt);

        view_databaseBt = new QPushButton(TcpServer);
        view_databaseBt->setObjectName(QStringLiteral("view_databaseBt"));
        view_databaseBt->setGeometry(QRect(9, 343, 75, 23));
        cancelBt = new QPushButton(TcpServer);
        cancelBt->setObjectName(QStringLiteral("cancelBt"));
        cancelBt->setGeometry(QRect(426, 10, 75, 23));

        retranslateUi(TcpServer);

        QMetaObject::connectSlotsByName(TcpServer);
    } // setupUi

    void retranslateUi(QWidget *TcpServer)
    {
        TcpServer->setWindowTitle(QApplication::translate("TcpServer", "TcpServer", 0));
        sendBt->setText(QApplication::translate("TcpServer", "\345\217\221\351\200\201", 0));
        sendBt_2->setText(QApplication::translate("TcpServer", "\346\270\205\351\231\244", 0));
        portEdit->setPlaceholderText(QApplication::translate("TcpServer", "\347\253\257\345\217\243\345\217\267", 0));
        startBt->setText(QApplication::translate("TcpServer", "\346\211\223\345\274\200", 0));
        view_databaseBt->setText(QApplication::translate("TcpServer", "\346\237\245\347\234\213\346\225\260\346\215\256\345\272\223", 0));
        cancelBt->setText(QApplication::translate("TcpServer", "\346\263\250\351\224\200", 0));
    } // retranslateUi

};

namespace Ui {
    class TcpServer: public Ui_TcpServer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TCPSERVER_H
