#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_usart.h"
#include "sys.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

static GPIO_InitTypeDef  	GPIO_InitStructure;
static USART_InitTypeDef 	USART_InitStructure;
static NVIC_InitTypeDef 	NVIC_InitStructure;		
static EXTI_InitTypeDef   EXTI_InitStructure;
static TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
static TIM_OCInitTypeDef  TIM_OCInitStructure;

static volatile uint8_t  g_usart1_recv_buf[128]={0};
static volatile uint32_t g_usart1_recv_cnt = 0;
static volatile uint32_t g_usart1_event=0;

static volatile uint8_t  g_usart3_recv_buf[128]={0};
static volatile uint32_t g_usart3_recv_cnt = 0;
static volatile uint32_t g_usart3_event=0;

//重定义fputc函数 
int fputc(int ch, FILE *f)
{ 	
	USART_SendData(USART1,ch);
	while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);  
	
	return ch;
}   

void delay_us(uint32_t nus)
{		
	uint32_t temp;	    	 
	SysTick->LOAD =SystemCoreClock/8/1000000*nus; 	//时间加载	  		 
	SysTick->VAL  =0x00;        					//清空计数器
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ; 		//使能滴答定时器开始倒数 	 
	do
	{
		temp=SysTick->CTRL;
	}while((temp&0x01)&&!(temp&(1<<16)));			//等待时间到达   
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk; 		//关闭计数器
	SysTick->VAL =0X00;       						//清空计数器 
}

void delay_ms(uint16_t nms)
{	 		  	  
	uint32_t temp;		   
	SysTick->LOAD=SystemCoreClock/8/1000*nms;		//时间加载(SysTick->LOAD为24bit)
	SysTick->VAL =0x00;           					//清空计数器
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ;    	//能滴答定时器开始倒数 
	do
	{
		temp=SysTick->CTRL;
	}while((temp&0x01)&&!(temp&(1<<16)));			//等待时间到达   
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;    	//关闭计数器
	SysTick->VAL =0X00;     		  				//清空计数器	  	    
} 

void LED_Init(void)
{    	 
  
	//使能GPIOE，GPIOF时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOF, ENABLE);			

	//GPIOF9,F10初始化设置 
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9 | GPIO_Pin_10;		//LED0和LED1对应IO口
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;			    	//普通输出模式，
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;					//推挽输出，驱动LED需要电流驱动
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		    	//100MHz
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;				    //上拉
	GPIO_Init(GPIOF, &GPIO_InitStructure);							//初始化GPIOF，把配置的数据写入寄存器						


	//GPIOE13,PE14初始化设置 
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_13 | GPIO_Pin_14;		//LED2和LED3对应IO口
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;					//普通输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;					//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;				//100MHz
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;					//上拉
	GPIO_Init(GPIOE, &GPIO_InitStructure);							//初始化GPIOE，把配置的数据写入寄存器

	GPIO_SetBits(GPIOF,GPIO_Pin_9  | GPIO_Pin_10);			    	//GPIOF9,PF10设置高，灯灭
	GPIO_SetBits(GPIOE,GPIO_Pin_13 | GPIO_Pin_14);		
}

void key_exti_init(void)
{
	
	/* GPIOA GPIOE 硬件时钟使能 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOE, ENABLE);

	/* 使能 SYSCFG 时钟 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);	
	
	/* 配置PA0引脚为输入模式  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;						//第0根引脚	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;				//输入模式，能够检测外部电平状态
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;	//GPIO最大的速度为100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上下拉电阻
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* 配置PE2引脚为输入模式  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;			//第2 根引脚	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;							//输入模式，能够检测外部电平状态
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;						//GPIO最大的速度为100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;							//使能内部上拉电阻（可选）
	GPIO_Init(GPIOE, &GPIO_InitStructure);


	/* Connect EXTI Line0 to PA0 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
	
	/* Connect EXTI Line2 to PE2 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOE, EXTI_PinSource2);


	/* Configure EXTI Line0 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;				//使用外部中断控制线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//中断事件
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;	//上升沿触发，按键松开的时候就触发中断  
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;				//中断控制线使能
	EXTI_Init(&EXTI_InitStructure);


	/* Configure EXTI Line2  */
	EXTI_InitStructure.EXTI_Line = EXTI_Line2;				//使用外部中断控制线2
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//中断事件
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;	//上升沿触发，按键松开的时候就触发中断  
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;				//中断控制线使能
	EXTI_Init(&EXTI_InitStructure);

	//支持4个抢占优先级，支持4个响应优先级
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	/* Enable and set EXTI Line0 Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;			//外部中断控制线0中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;//抢占优先级0x3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;		//响应优先级0x3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				//外部中断控制线0中断通道使能
	NVIC_Init(&NVIC_InitStructure);


	/* Enable and set EXTI Line2 Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;			//外部中断控制线2中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x3;//抢占优先级0x3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x3;		//响应优先级0x3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;				//外部中断控制线2中断通道使能
	NVIC_Init(&NVIC_InitStructure);
}

void tim13_init(uint32_t freq)
{
	/* TIM13 clock enable,使能定时器13硬件时钟 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13, ENABLE);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);		

	/* 配置PF8引脚为复用功能模式  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;			//第8根引脚	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;		//复用功能模式，使用引脚的第二功能
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		//推挽输出模式
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;	//GPIO最大的速度为100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		//使能内部上拉电阻，增大输出电流的能力
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	
	/* Connect TIM13 pins to PF8 */  
	GPIO_PinAFConfig(GPIOF, GPIO_PinSource8, GPIO_AF_TIM13);
	
	
	/* Time base configuration，定时器参数的基本配置：定时时间、分频值、计数方法*/
	TIM_TimeBaseStructure.TIM_Period =(10000/freq)-1;														//设置频率
	TIM_TimeBaseStructure.TIM_Prescaler = 8400-1;												//预分频值8400
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;						//二次分频，也就是说再进行一次分频，当前是1分频
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;			//向上计数的方法

	TIM_TimeBaseInit(TIM13, &TIM_TimeBaseStructure);
	
	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;					//PWM模式1,在递增计数模式下，只要 TIMx_CNT<TIMx_CCR1，通道 1 便为有效状态，否则为无效状态。
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;		//允许输出
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;			//有效的时候，输出高电平
	TIM_OC1Init(TIM13, &TIM_OCInitStructure);

	TIM_OC1PreloadConfig(TIM13, TIM_OCPreload_Enable);					//自动重载初值，不断输出PWM脉冲
	TIM_ARRPreloadConfig(TIM13, ENABLE);								//自动重载使能

	/* TIM13 enable counter，使能定时器13工作 */
	TIM_Cmd(TIM13, ENABLE);

}

void tim13_deinit(void)
{
	
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Disable;			//禁止输出
	TIM_OC1Init(TIM13, &TIM_OCInitStructure);

	/* TIM13 disable counter，禁止定时器13工作 */
	TIM_Cmd(TIM13, DISABLE);
}

void USART1_Init(uint32_t baud)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE); 							//使能GPIOA时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);							//使能USART1时钟
 
	//串口1对应引脚复用映射
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource9,GPIO_AF_USART1); 						//GPIOA9复用为USART1
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource10,GPIO_AF_USART1); 						//GPIOA10复用为USART1
	
	//USART1端口配置
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; 						//GPIOA9与GPIOA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;									//复用功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;								//速度50MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 									//推挽复用输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; 									//上拉
	GPIO_Init(GPIOA,&GPIO_InitStructure); 											//初始化PA9，PA10

	//USART1 初始化设置
	USART_InitStructure.USART_BaudRate = baud;										//波特率设置
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//收发模式
	USART_Init(USART1, &USART_InitStructure); 										//初始化串口1
	
	USART_Cmd(USART1, ENABLE);  													//使能串口1 
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);									//开启相关中断

	//Usart1 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;								//串口1中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;							//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority =3;								//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;									//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);													//根据指定的参数初始化VIC寄存器
}

void USART3_Init(uint32_t baud)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* GPIOB Configuration: PB10 PB11 */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	/* Connect USART3_TX pins to PB10 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
	
	/* Connect USART3_RX pins to PB11 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);

	/* Enable USART3 clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	USART_InitStructure.USART_BaudRate = baud;									
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;				
	USART_InitStructure.USART_StopBits = USART_StopBits_1;				
	USART_InitStructure.USART_Parity = USART_Parity_No;						
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					
	USART_Init(USART3, &USART_InitStructure);

	 
	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	 
	/* Enable USART3 */
	USART_Cmd(USART3, ENABLE);
	
	/* Enable the Rx buffer empty interrupt */
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
}

void USART1_SendBytes(uint8_t *pbuf,uint32_t len)
{

	while(len--)
	{
		USART_SendData(USART1,*pbuf++);
		while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET); 	
	}

}

void USART1_SendString(uint8_t *pstr)
{
	while(pstr && *pstr)
	{
		USART_SendData(USART1,*pstr++);
		while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET); 		
	}
}

void USART3_SendBytes(uint8_t *pbuf,uint32_t len)
{

	while(len--)
	{
		USART_SendData(USART3,*pbuf++);
		while(USART_GetFlagStatus(USART3,USART_FLAG_TXE)==RESET); 	
	}

}

void USART3_SendString(uint8_t *pstr)
{
	while(pstr && *pstr)
	{
		USART_SendData(USART3,*pstr++);
		while(USART_GetFlagStatus(USART3,USART_FLAG_TXE)==RESET); 		
	}
}


void sr04_init(void)
{
	//使能GPIOB，GPIOE时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOE, ENABLE);			

	//PB6初始化设置 
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6;						//PB6引脚
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;			    	//普通输出模式，
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;					//推挽输出，驱动LED需要电流驱动
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		    	//100MHz
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;				    //上拉
	GPIO_Init(GPIOB, &GPIO_InitStructure);							//初始化GPIOB，把配置的数据写入寄存器						

	//PE6初始化设置 
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_6;						//PB6引脚
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;			    	//普通输入模式，
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		    	//100MHz
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;				    //上拉
	GPIO_Init(GPIOE, &GPIO_InitStructure);							//初始化GPIOE，把配置的数据写入寄存器		


}

//获取距离，单位为mm
uint32_t sr04_get_distance(void)
{
	uint32_t t=0;
	
	//发送触发信号
	PBout(6)=1;
	delay_us(20);
	PBout(6)=0;
	
	//等待输出回响信号
	while(PEin(6)==0);
	
	
	//测量回响信号时间
	
	while(PEin(6))
	{
		delay_us(8);
		
		t++;
	
	}
	
	t = t/2;

	
	return (3*t);

}
void dht11_outputmode(void)
{

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOG, &GPIO_InitStructure);	
	
}

void dht11_inputmode(void)
{
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOG, &GPIO_InitStructure);	
}

int dht11_start(void)
{
	uint32_t i;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
	
	//PG9变为输出模式
	dht11_outputmode();

	//PG9引脚输出低电平
	PGout(9) = 0;

	//延时20ms
	delay_ms(20);

	//PG9引脚输出高电平
	PGout(9) = 1;
	
	//延时30us
	delay_us(30);

	//切换到输入模式，检查DHT11温湿度模块是否有响应
	dht11_inputmode();
	
	//检测是否有低电平
	i=0;
	while(i<100)
	{
		if(PGin(9)==0)
			break;
		delay_us(1);
		i++;
	}
	
	//超时
	if(i >=100)
		return 1;
	
	
	//检测是否有高电平
	i=0;
	while(i<100)
	{
		if(PGin(9))
			break;
		delay_us(1);
		i++;
	}
	
	//超时
	if(i >=100)
		return 1;
	
	//没有错误，正确的返回 
	return 0;
}

uint8_t dht11_read_byte(void)
{
	uint8_t d=0;
	
	uint32_t i=0;
	
	//等待高电平持续完毕
	while(PGin(9));
	
	for(i=0; i<8; i++)
	{
		//检测低电平
		while(PGin(9)==0);
		
		//延时40us
		delay_us(40);
		
		//判断PG9引脚的电平,若为高电平就是bit1
		if(PGin(9))
		{
			d|=1<<(7-i);
			
			//等待高电平持续完毕
			while(PGin(9));		
		}
	}
		
	return d;
}



uint32_t dht11_read_data(uint8_t *pbuf)
{
	uint32_t i=0;
	uint8_t  check_sum=0;
	
	//建立启动信号
	while(dht11_start()==1);
	
	//连续读取5个字节
	for(i=0; i<5; i++)
	{
		pbuf[i] = dht11_read_byte();
	
	}
	//计算校验和
	check_sum = pbuf[0]+pbuf[1]+pbuf[2]+pbuf[3];
	
	//检验和判断
	if(check_sum != pbuf[4])
		return 1;
	
	return 0;
}
int main(void)
{ char buf[64]={0};
	uint32_t distance;
	uint32_t time_cnt=0;
	uint8_t dht11_data[5]={0};
//使能GPIOG的硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
	
	LED_Init();		

	//系统定时器初始化，时钟源来自HCLK，且进行8分频，
	//系统定时器时钟频率=168MHz/8=21MHz
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8); 
		
	//设置中断优先级分组2
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	//串口1,波特率115200bps,开启接收中断
	USART1_Init(115200);
	USART3_Init(9600);
	
	//初始化超声波模块
	sr04_init();

	//按键中断初始化
	key_exti_init();
	
	while(1)
	{
						delay_ms(3000);
							distance = sr04_get_distance();
					if(dht11_read_data(dht11_data)==0)
					{	
							printf("temp=%d.%d\r\n",dht11_data[2],dht11_data[3]);
							printf("humi=%d.%d\r\n",dht11_data[0],dht11_data[1]);
							printf("\r\n");
					}
						sprintf(buf,"%d.%d-%d.%d-%d\r\n", dht11_data[0],dht11_data[1],dht11_data[2],dht11_data[3],distance);
						USART3_SendString(buf);
						printf("%s\r\n",buf);
						delay_ms(3000);
					}

}

void USART1_IRQHandler(void)                				//串口1中断服务程序
{
	uint8_t d;
	

	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  	//接收中断
	{
		
	
	} 
} 
	
void USART3_IRQHandler(void)
{
	uint8_t d;
	
	/* USART in Receiver mode */
	if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET)  
	{
		delay_ms(5000);
#if 1
		//从串口3接收数据
		d=g_usart3_recv_buf[g_usart3_recv_cnt]=USART_ReceiveData(USART3);	
		
		g_usart3_recv_cnt++;
		
		//检测到换行符或接收的数据满的时候则发送数据
		if(g_usart3_recv_buf[g_usart3_recv_cnt-1]=='\n' || g_usart3_recv_cnt>=(sizeof g_usart3_recv_buf)-1)
		{
			
			g_usart3_event = 1;
					
		}
#else
		d=USART_ReceiveData(USART3);
		USART1_SendBytes(&d,1);
#endif
	}
}



